package com.enel.bajaordejec;

import com.enel.bajaordejec.helper.EjecutorProcesoExterno;
import com.enel.bajaordejec.helper.GeneradorPlano;
import com.enel.bajaordejec.helper.ProcesadorEntrada;
import com.enel.bajaordejec.helper.ProcesadorSalida;
import com.enel.bajaordejec.model.Orden;
import com.enel.bajaordejec.model.ParametrosProceso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BajaOrdEjecApplication implements CommandLineRunner {

	@Autowired
	private ProcesoPrincipal procesoPrincipal;
	
	public static void main(String[] args) {
		SpringApplication.run(BajaOrdEjecApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		ProcesadorEntrada.procesar(args);
		ParametrosProceso parametros = procesoPrincipal.obtenerDatosIniciales();
		GeneradorPlano.crearArchivo(
				parametros.getPathSalida(), parametros.getFechaPago());
		List<Orden> ordenes = 
				procesoPrincipal.obtenerOrdenes(parametros.getFechaProceso());	
		procesoPrincipal.procesarOrdenes(ordenes);
		GeneradorPlano.moverArchivo(parametros.getPathSalida(), parametros.getPathDestino());
		EjecutorProcesoExterno.iniciarEjecucion(parametros);
		ProcesadorSalida.finalizarPrograma();
	}
}
