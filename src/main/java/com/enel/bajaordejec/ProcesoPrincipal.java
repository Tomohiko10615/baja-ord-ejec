package com.enel.bajaordejec;

import com.enel.bajaordejec.dto.OrdenDTO;
import com.enel.bajaordejec.helper.GeneradorLinea;
import com.enel.bajaordejec.helper.GeneradorPlano;
import com.enel.bajaordejec.helper.ProcesadorEntrada;
import com.enel.bajaordejec.model.Linea;
import com.enel.bajaordejec.model.Orden;
import com.enel.bajaordejec.model.ParametrosProceso;
import com.enel.bajaordejec.service.OrdOrdenService;
import com.enel.bajaordejec.service.TablaService;
import com.enel.bajaordejec.service.WamOrdPasadasService;
import com.enel.bajaordejec.service.WamOrdProcesadasService;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

@Component
public class ProcesoPrincipal {
	
	@Autowired
    private PlatformTransactionManager transactionManager;
	private TransactionStatus transaction;
	
	@Autowired
	private TablaService tablaService;
	@Autowired
	private WamOrdPasadasService wamOrdPasadasService;
	@Autowired
	private WamOrdProcesadasService wamOrdProcesadasService;
	@Autowired
	private OrdOrdenService ordOrdenService;

	private Linea linea = new Linea();
	private int nroOrdenesProcesadas;

	public ParametrosProceso obtenerDatosIniciales() {
		String pathEjecutable = tablaService.obtenerPathEjecutable();
		ParametrosProceso parametros = tablaService.obtenerParametrosProceso();
		parametros.setFechaProceso(ProcesadorEntrada.getFechaProceso());
		parametros.setPathEjecutable(pathEjecutable);
		return parametros;
	}
	
	public List<Orden> obtenerOrdenes(String fechaProceso) {
		wamOrdProcesadasService.callObtenerOrdenesMorocidad(fechaProceso);
		List<OrdenDTO> ordenesTelegestion = wamOrdProcesadasService.obtenerOrdenesTelegestion(fechaProceso);
		return ordOrdenService.filtrarOrdenes(ordenesTelegestion);
	}

	public void procesarOrdenes(List<Orden> ordenes) {
		transaction = transactionManager.getTransaction(null);
		ordenes = removerDuplicados(ordenes);
		for (Orden orden : ordenes) {
			nroOrdenesProcesadas++;
			BeanUtils.copyProperties(orden, linea);
			GeneradorPlano.imprimirLinea(GeneradorLinea.obtenerLineaTexto(linea));
			actualizarEstadoOrden(orden.getIdOrden());
		}
		transactionManager.commit(transaction);
		transactionManager.rollback(transaction);
	}

	private List<Orden> removerDuplicados(List<Orden> ordenes) {
		return ordenes.stream().distinct().collect(Collectors.toList());
	}

	private void actualizarEstadoOrden(Long idOrden) {
		wamOrdPasadasService.actualizarOrdenesPasadas(idOrden);
		wamOrdProcesadasService.actualizarOrdenesProcesadas(idOrden);
		if (nroOrdenesProcesadas % 10 == 0) {
			transactionManager.commit(transaction);
		}
	}
}
