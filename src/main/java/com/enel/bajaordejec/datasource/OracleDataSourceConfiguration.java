package com.enel.bajaordejec.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.enel.bajaordejec.repository.oracle", 
entityManagerFactoryRef = "synergiaEntityManagerFactory", transactionManagerRef = "synergiaTransactionManager")
public class OracleDataSourceConfiguration {

	@Bean(name = "synergiaDataSource")
	public DataSource synergiaDataSource() {
		BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("ORCL");
		HikariConfig dataSourceBuilder = new HikariConfig();
        dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
        dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
        dataSourceBuilder.setUsername(beanConn.getDbUser());
        dataSourceBuilder.setPassword(beanConn.getDbPass());        
        dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
        dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
        return dataSource;
    }

	@Bean(name = "synergiaEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean synergiaEntityManagerFactory(
			EntityManagerFactoryBuilder synergiaEntityManagerFactoryBuilder,
			@Qualifier("synergiaDataSource") DataSource synergiaDataSource) {

		Map<String, String> synergiaJpaProperties = new HashMap<>();
		synergiaJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");

		return synergiaEntityManagerFactoryBuilder.dataSource(synergiaDataSource)
				.packages("com.enel.bajaordejec.entity.oracle").persistenceUnit("synergiaDataSource")
				.properties(synergiaJpaProperties).build();
	}

	@Bean(name = "synergiaTransactionManager")
	public PlatformTransactionManager synergiaTransactionManager(
			@Qualifier("synergiaEntityManagerFactory") EntityManagerFactory synergiaEntityManagerFactory) {

		return new JpaTransactionManager(synergiaEntityManagerFactory);
	}

}
