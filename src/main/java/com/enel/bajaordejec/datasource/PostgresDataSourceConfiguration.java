package com.enel.bajaordejec.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.enel.bajaordejec.repository.postgres", 
entityManagerFactoryRef = "scomEntityManagerFactory", transactionManagerRef = "scomTransactionManager")
public class PostgresDataSourceConfiguration {

	@Primary
	@Bean(name = "scomDataSource")
	public DataSource scomDataSource() {
		BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("POST");
		HikariConfig dataSourceBuilder = new HikariConfig();
        dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
        dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
        dataSourceBuilder.setUsername(beanConn.getDbUser());
        dataSourceBuilder.setPassword(beanConn.getDbPass());        
        dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
        dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
        HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
        return dataSource;
	}

	@Primary
	@Bean(name = "scomEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean scomEntityManagerFactory(
			EntityManagerFactoryBuilder scomEntityManagerFactoryBuilder,
			@Qualifier("scomDataSource") DataSource scomDataSource) {

		Map<String, String> scomJpaProperties = new HashMap<>();
		scomJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		
		return scomEntityManagerFactoryBuilder.dataSource(scomDataSource)
				.packages("com.enel.bajaordejec.entity.postgres").persistenceUnit("scomDataSource")
				.properties(scomJpaProperties).build();
	}

	@Primary
	@Bean(name = "scomTransactionManager")
	public PlatformTransactionManager scomTransactionManager(
			@Qualifier("scomEntityManagerFactory") EntityManagerFactory scomEntityManagerFactory) {

		return new JpaTransactionManager(scomEntityManagerFactory);
	}

}
