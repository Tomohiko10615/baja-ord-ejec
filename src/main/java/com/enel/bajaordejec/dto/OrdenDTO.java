package com.enel.bajaordejec.dto;

public interface OrdenDTO {
	
	String getNroOrden();
    String getCodTipoCorte();
    String getCodEjecutor();
    Long getNroServicio();
    Long getNroComponente();
    String getCodMarcaComp();
    String getCodModeloMed();
    String getCodSitTerrenoPrim();
    String getCodSitTerrenoSec();
    String getCodAccRealizada();
    String getFechaEjecucion();
    String getObservacion();
    Double getLectura();
    Long getIdOrden();
    String getAccRealizadaCor();
    
    String setNroOrden(String nroOrden);
}
