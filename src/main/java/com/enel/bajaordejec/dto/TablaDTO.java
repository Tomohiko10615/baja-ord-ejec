package com.enel.bajaordejec.dto;

public interface TablaDTO {

	String getPathSalida();
	String getFechaPago();
	String getPathDestino();
}
