package com.enel.bajaordejec.entity.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ord_orden")
public class OrdOrden {
	
	@Id
	@Column(name = "id_orden")
	private Long idOrden;

}
