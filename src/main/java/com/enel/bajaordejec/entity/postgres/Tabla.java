package com.enel.bajaordejec.entity.postgres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "tabla")
public class Tabla {

	@Id
	private String codigo;
	
	@Column(name = "valor_alf")
	private String valorAlf;
	
	private String descripcion;
	
	private String sucursal;
	
	@Column(name = "fecha_activacion")
	private Date fechaActivacion;
	
	@Column(name = "fecha_desactivac")
	private Date fechaDesactivac;
}
