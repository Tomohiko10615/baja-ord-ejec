package com.enel.bajaordejec.entity.postgres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "wam_ord_pasadas")
public class WamOrdPasadas {

	@Id
	@Column(name = "id_orden")
	private Long idOrden;
}
