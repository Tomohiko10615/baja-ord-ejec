package com.enel.bajaordejec.helper;

import static com.enel.bajaordejec.helper.constants.Generics.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.enel.bajaordejec.model.ParametrosProceso;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class EjecutorProcesoExterno {
	
	private EjecutorProcesoExterno() {
		super();
	}

	private static final String COD_EMPRESA = "EDEL";
	private static final String EXPLOTA_CORTE = "EXPLOTACORTE";

	public static void iniciarEjecucion(ParametrosProceso parametros) throws InterruptedException {

		ProcessBuilder pb = new ProcessBuilder(
        		"java",
        		"-jar",
        		parametros.getPathEjecutable() + "/MORRecepcionMasivaOrdenes.jar",
        		COD_EMPRESA,
        		parametros.getPathDestino(),
        		GeneradorPlano.getNombreArchivo(),
        		EXPLOTA_CORTE,
        		parametros.getPathSalida()
        		);
		
		log.info(parametros.getPathEjecutable());
		log.info(parametros.getPathDestino());
		log.info(GeneradorPlano.getNombreArchivo());
		log.info(parametros.getPathSalida());
		
        Process process = null;
        
		try {
			process = pb.start();
			imprimirSalidaConsola(process);
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al ejecutar el proceso externo.");
		}
		validarEjecucion(process.exitValue());
	}

	private static void imprimirSalidaConsola(Process process) {
		InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
		try {
			while ((line = br.readLine()) != null) {
				log.info(line);
			}
		} catch (IOException e) {
			log.error("Error al imprimir línea.");
			e.printStackTrace();
		}
	}
	
	private static void validarEjecucion(int exitValue) {
		if (exitValue == SUCCESS) {
			log.info("Proceso se ejecutó con éxito");
        } else {
        	log.info("Proceso no se ejecutó con éxito");
		}
	}
}
