package com.enel.bajaordejec.helper;

import com.enel.bajaordejec.model.Linea;

public final class GeneradorLinea {

	private GeneradorLinea() {
		super();
	}
	
	private static final String SEPARADOR = ";";
	
	public static String obtenerLineaTexto(Linea linea) {
		return linea.getNroOrden()
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodTipoCorte()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodEjecutor()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getNroServicio().toString()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getNroComponente().toString()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodMarcaComp()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodModeloMed()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodSitTerrenoPrim()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodSitTerrenoSec()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getCodAccRealizada()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getFechaEjecucion()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getObservacion()))
				.concat(SEPARADOR)
				.concat(convertirNulls(linea.getLectura().toString()));
	}

	private static String convertirNulls(String campo) {
		return campo == null ? "" : campo;
	}
}
