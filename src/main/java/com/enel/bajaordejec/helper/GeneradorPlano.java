package com.enel.bajaordejec.helper;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class GeneradorPlano {

	private GeneradorPlano() {
		super();
	}

	private static String file;
	private static final String FILE_NAME = "mor";
	private static String nombreArchivo;
	
	public static void crearArchivo(String pathSalida, String fechaPago) {
		try {
			nombreArchivo = FILE_NAME + fechaPago + ".txt";
			file = pathSalida + nombreArchivo;
			Path logFilePath = Paths.get(file);
		    Files.createFile(logFilePath);
		} catch (IOException e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al crear el archivo.");
		}
	}
	
	public static void imprimirLinea(String line) {
		log.info(line);
		try(PrintWriter logWriter = 
				new PrintWriter(
						new FileWriter(file, true))) {
			logWriter.write(line);
			logWriter.println();
		} catch (IOException e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al escribir en el archivo.");
		}
	}

	public static String getNombreArchivo() {
		return nombreArchivo;
	}

	public static void moverArchivo(String pathSalida, String pathDestino) {
		String comando = "cp "
				.concat(pathSalida)
				.concat(nombreArchivo)
				.concat(" ")
				.concat(pathDestino);
		
		Process process;
		try {
			process = Runtime.getRuntime().exec(new String[]{"bash","-c",comando});
			process.waitFor();
			if (process.exitValue() != 0) {
				log.error("No se pudo ejecutar el comando: {}", comando);
				System.exit(1);
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
			ProcesadorSalida.finalizarPrograma("Error al mover el archivo.");
		}
	}
}
