package com.enel.bajaordejec.helper;
import static com.enel.bajaordejec.helper.constants.Generics.*;

public final class ProcesadorEntrada {

	private static String fechaProceso;
	
	private ProcesadorEntrada() {
		super();
	}
	
	public static void procesar(String[] parametrosEntrada) {
		validar(parametrosEntrada);
		setearValores(parametrosEntrada);
	}

	public static void validar(String[] parametrosEntrada) {
		if (parametrosEntrada.length != NRO_PARAMETROS_ENTRADA) {
			ProcesadorSalida.finalizarPrograma("Parámetros de entrada incorrectos");
		}
	}
	
	private static void setearValores(String[] parametrosEntrada) {
		fechaProceso = parametrosEntrada[0];
	}

	public static String getFechaProceso() {
		return fechaProceso;
	}
}
