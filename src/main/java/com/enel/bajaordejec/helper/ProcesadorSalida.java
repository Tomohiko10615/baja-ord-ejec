package com.enel.bajaordejec.helper;

import static com.enel.bajaordejec.helper.constants.Generics.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProcesadorSalida {

	private ProcesadorSalida() {
		super();
	}

	public static void finalizarPrograma(String msjError) {
		log.error(msjError);
		System.exit(FAIL);
	}
	
	public static void finalizarPrograma() {
		System.exit(SUCCESS);
	}
}
