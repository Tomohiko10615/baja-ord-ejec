package com.enel.bajaordejec.helper.constants;

public class Generics {

	private Generics() {
		super();
	}

	public static final int NRO_PARAMETROS_ENTRADA = 1;
	public static final int SUCCESS = 0;
	public static final int FAIL = 1;
}
