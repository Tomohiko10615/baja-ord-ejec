package com.enel.bajaordejec.helper.constants;

public class Schemas {

	private Schemas() {
		super();
	}
	public static final String SCHSCOM = "schscom.";
	public static final String TELEGESTION = "telegestion.";
}
