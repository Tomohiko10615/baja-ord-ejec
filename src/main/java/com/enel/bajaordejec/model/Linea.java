package com.enel.bajaordejec.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Linea {

	private String nroOrden;
	private Long nroServicio;
	private Long nroComponente;
	private String codTipoCorte;
	private String codEjecutor;
	private String codMarcaComp;
	private String codModeloMed;
	private String codSitTerrenoPrim;
	private String codSitTerrenoSec;
	private String codAccRealizada;
	private String fechaEjecucion;
	private String observacion;
	private Double lectura;
}
