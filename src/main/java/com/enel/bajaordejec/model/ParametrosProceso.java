package com.enel.bajaordejec.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParametrosProceso {

	private String fechaProceso;
	private String pathSalida;
	private String fechaPago;
	private String pathDestino;
	private String pathEjecutable;
}
