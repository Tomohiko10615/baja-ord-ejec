package com.enel.bajaordejec.repository.postgres;

import com.enel.bajaordejec.dto.TablaDTO;
import com.enel.bajaordejec.entity.postgres.Tabla;
import com.enel.bajaordejec.helper.constants.Schemas;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TablaRepository extends JpaRepository<Tabla, String> {
	
	@Query(value = 
    		"SELECT trim(valor_alf) as pathSalida, to_char(current_timestamp,'YYYYMMDDHH24MISS') as fechaPago, trim(descripcion) as pathDestino "
    		+ "  FROM " + Schemas.SCHSCOM + "TABLA "
    		+ "  WHERE nomtabla='PATH' "
    		+ "  AND codigo='ACTMOR' "
    		+ "  AND sucursal='0000' "
    		+ "  AND fecha_activacion <= to_date(to_char(current_date,'DD/MM/YYYY'),'DD/MM/YYYY') "
    		+ "  AND (fecha_desactivac > to_date(to_char(current_date,'DD/MM/YYYY'),'DD/MM/YYYY') "
    		+ "  OR fecha_desactivac is null)"
    	    , nativeQuery = true)
	TablaDTO obtenerParametrosProceso();

	@Query(value = 
    		"SELECT valor_alf "
    		+ "  FROM " + Schemas.SCHSCOM + "TABLA "
    		+ "  WHERE nomtabla='PATH' "
    		+ "  AND codigo='EXES' "
    		+ "  AND sucursal='0000' "
    		+ "  AND fecha_activacion <= to_date(to_char(current_date,'DD/MM/YYYY'),'DD/MM/YYYY') "
    		+ "  AND (fecha_desactivac > to_date(to_char(current_date,'DD/MM/YYYY'),'DD/MM/YYYY') "
    		+ "  OR fecha_desactivac is null)"
    	    , nativeQuery = true)
	String obtenerPathEjecutable();

}
