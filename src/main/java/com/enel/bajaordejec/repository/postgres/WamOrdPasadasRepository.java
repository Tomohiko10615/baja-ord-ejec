package com.enel.bajaordejec.repository.postgres;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.bajaordejec.entity.postgres.WamOrdPasadas;
import com.enel.bajaordejec.helper.constants.Schemas;

@Repository
public interface WamOrdPasadasRepository extends JpaRepository<WamOrdPasadas, Long> {

	@Modifying
	@Query(value = 
    		"UPDATE " + Schemas.TELEGESTION + "WAM_ORD_PASADAS "
    		+ "  SET estado=3 "
    		+ "  WHERE id_orden=?"
    	    , nativeQuery = true)
	void actualizarEstado(Long idOrden);

}
