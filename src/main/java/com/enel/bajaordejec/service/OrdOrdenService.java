package com.enel.bajaordejec.service;

import com.enel.bajaordejec.dto.OrdenDTO;
import com.enel.bajaordejec.helper.ProcesadorSalida;
import com.enel.bajaordejec.model.Orden;
import com.enel.bajaordejec.repository.oracle.OrdOrdenRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class OrdOrdenService {

	private final OrdOrdenRepository ordOrdenRepository;

	public List<Orden> filtrarOrdenes(List<OrdenDTO> ordenesTelegestion) {
		List<Orden> ordenes = new ArrayList<>();
		Orden orden = null;
		OrdenDTO ordenFiltrada = null;
		for (OrdenDTO ordenTelegestion : ordenesTelegestion) {
			orden = new Orden();
			try {
				ordenFiltrada = ordOrdenRepository.obtenerOrdenesFiltradas(
						ordenTelegestion.getIdOrden(), 
						ordenTelegestion.getAccRealizadaCor());
			} catch (Exception e) {
				e.printStackTrace();
				ProcesadorSalida.finalizarPrograma("Error al obtener orden filtrada.");
			}
			if (ordenFiltrada != null) {
				log.info("Nro orden: {}", ordenFiltrada.getNroOrden());
				log.info("id orden: {}", ordenTelegestion.getIdOrden());
				BeanUtils.copyProperties(ordenTelegestion, orden);
				orden.setNroOrden(ordenFiltrada.getNroOrden());
				log.info("Orden: {}", orden.toString());
				ordenes.add(orden);
			}
		}
		if (ordenes.isEmpty()) {
			ProcesadorSalida.finalizarPrograma("No se encontraron órdenes filtradas a procesar.");
		}
		log.info("Ordenes: {}", ordenes.toString());
		return ordenes;
	}
}
