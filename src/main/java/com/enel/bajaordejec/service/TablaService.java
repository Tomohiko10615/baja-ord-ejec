package com.enel.bajaordejec.service;

import com.enel.bajaordejec.dto.TablaDTO;
import com.enel.bajaordejec.helper.ProcesadorSalida;
import com.enel.bajaordejec.model.ParametrosProceso;
import com.enel.bajaordejec.repository.postgres.TablaRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TablaService {

	private final TablaRepository tablaRepository;

	public ParametrosProceso obtenerParametrosProceso() {
		TablaDTO tablaDTO = null;
		try {
			tablaDTO = tablaRepository.obtenerParametrosProceso();
		} catch (Exception e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al obtener los parámetros de proceso");
		}
		if (tablaDTO == null) {
			ProcesadorSalida.finalizarPrograma("No se encontraron resultados");
		}
		return ParametrosProceso.builder()
				.pathSalida(tablaDTO.getPathSalida())
				.fechaPago(tablaDTO.getFechaPago())
				.pathDestino(tablaDTO.getPathDestino())
				.build();
	}

	public String obtenerPathEjecutable() {
		String pathEjecutable = "";
		try {
			pathEjecutable = tablaRepository.obtenerPathEjecutable();
		} catch (Exception e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al obtener el path ejecutable");
		}
		return pathEjecutable;
	}
}
