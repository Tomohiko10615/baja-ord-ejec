package com.enel.bajaordejec.service;

import com.enel.bajaordejec.helper.ProcesadorSalida;
import com.enel.bajaordejec.repository.postgres.WamOrdPasadasRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WamOrdPasadasService {

	private final WamOrdPasadasRepository wamOrdPasadasRepository;

	public void actualizarOrdenesPasadas(Long idOrden) {
		try {
			wamOrdPasadasRepository.actualizarEstado(idOrden);
		} catch (Exception e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al actualizar órdenes pasadas.");
		}
	}

}
