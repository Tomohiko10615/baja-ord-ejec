package com.enel.bajaordejec.service;

import com.enel.bajaordejec.dto.OrdenDTO;
import com.enel.bajaordejec.helper.ProcesadorSalida;
import com.enel.bajaordejec.repository.postgres.WamOrdProcesadasRepository;

import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WamOrdProcesadasService {

	private final WamOrdProcesadasRepository wamOrdProcesadasRepository;
	
	public void callObtenerOrdenesMorocidad(String fechaProceso) {
		try {
			wamOrdProcesadasRepository.callObtenerOrdenesMorocidad(fechaProceso);
		} catch (Exception e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al ejecutar stored procedure obtenerOrdenesMorosidad.");
		}
	}

	public void actualizarOrdenesProcesadas(Long idOrden) {
		try {
			wamOrdProcesadasRepository.actualizarEstado(idOrden);
		} catch (Exception e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al actualizar órdenes procesadas.");
		}
	}

	public List<OrdenDTO> obtenerOrdenesTelegestion(String fechaProceso) {
		List<OrdenDTO> ordenes = null;
		try {
			ordenes = wamOrdProcesadasRepository.obtenerOrdenes(fechaProceso);
		} catch (Exception e) {
			e.printStackTrace();
			ProcesadorSalida.finalizarPrograma("Error al obtener las órdenes de telegestión.");
		}
		if (ordenes.isEmpty()) {
			ProcesadorSalida.finalizarPrograma("No se encontraron órdenes a procesar en telegestión.");
		}
		return ordenes;
	}

}
